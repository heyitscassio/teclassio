# Teclássio keyboard

![teclassio](doc/teclassio.png)

# **UNTESTED!!!**

The Teclássio is a 30 + 6 key, column splayed, column staggered, split ergonomic
keyboard. It is based on the GD32VF103CBT6 RISC-V MCU.

It is heavily inspired by the ferris, yaemk, architeuthis dux and 3W6 keyboards. It borrows schematic pieces on all those keyboards.

# Goals

- have a small footprint.
- relatively cheap.
- simple, no leds, oleds, trackball, etc..
- be possible to assemble by hand, all kicad footprints are the "hand solder"
version (when necessary).

# TODO:

## Rev 0.1

- [X] Check schematic (again)
- [X] Check BOM (again)
- [X] Place the ground vias in a better way (again)
- [ ] Order PCB
- - [ ] Order components
	- [X] GD32VF103CBT6 MCU
	- [X] LCSC order
- [ ] Profit

## Rev 0.2

- [ ] Better ESD protection on the I2C USB
- [ ] Better over voltage I2C USB, in the master side

# Other projects

This keyboard would not exist 
without the following projects:

3W6 keyboard:
https://github.com/weteor/3W6

Ergogen ergonomic keyboard generator:
https://github.com/ergogen/ergogen

Ferris keyboard:
https://github.com/pierrechevalier83/ferris

Fifi keyboard:
https://github.com/raychengy/fifi_split_keeb

YAEMK keyboard:
https://karlk90.github.io/yaemk-split-kb

Longan Nano development board
http://longan.sipeed.com/en/
